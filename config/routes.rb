Rails.application.routes.draw do
  devise_for :users
  resources :lists
  resources :tasks do 
    patch :mark_completed, on: :member
  end

  root to: 'lists#index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
